(function() {
	//Template
	var sourceSection = $('#entry-template').html();
	var templateSection;
	var htmlSection, posting;
	var locationsDB;
	var safari = false;

	try {
		sessionStorage.finalCoords = sessionStorage.finalCoords || 0;
	}
	catch(e){
		console.log("Your Browser doesn't support storage");
		safari = true;
	}
	//var sourceSection = $('#entry-template-info').html();
	$.get('http://localhost:3000/api/content/control', function( data ) {
		var contentToRender = data;
		delete contentToRender._id;
		delete contentToRender.identifier;
		templateSection = Handlebars.compile(sourceSection);
		//inyectar
		htmlSection = templateSection(contentToRender);
		locationsDB = contentToRender.location || 0;
		if( safari && locationsDB ) { 
			posting = $.post( '/noMapControl', { location: JSON.stringify(locationsDB) } );
		}
		//Compilar
		if($('.sections').length) {
			////insertar 
			$('.sections').html(htmlSection);
			$('.sections ul li').css("border-color",contentToRender.color);
			$('.sections a.headerRef,.sections .headerRef span').css("color",contentToRender.color);
		}
		if($('.info.info-services').length) {
			////insertar 
			$('.info').html(htmlSection);
			$('.info ul li').css("border-color",contentToRender.color);
		}
		if($('.info-concret.info').length) {
			////insertar 
			$('.info-concret').html(htmlSection);
		}
		if($('.info-concret.locations').length) {
			////insertar 
			$('.info-concret.locations').html(htmlSection);
		}
	}).fail(function() {
			console.log( "Error en la llamada" );
		});
	$(document).on('click','#ubicacion',function () {
		var locationToSend = JSON.stringify(locationsDB);
		sessionStorage.finalCoords = locationToSend;
	});
/////////////////////////
///Barra de navegacion///
////////////////////////
var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substrRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
}
	var direction = {};
	var states = [];

	$.get('http://localhost:3000/api/content/directions', function( data ) {
			delete data._id;
			direction = data;
		}).done(function() {
		    for (var i in direction) {
				states.push(direction[i].title);
			}
		  }).fail(function() {
				console.log( "Error en la llamada" );
			});

$('#the-basics').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'states',
  source: substringMatcher(states)
});

	$('.header-searchbar').on('click', '.tt-suggestion', function() {
		var destino = $('#the-basics').typeahead('val');
		for (var i in direction) {
			if (destino === direction[i].title) {
				window.location.replace(direction[i].ref);
			}
		}
	});

})($);